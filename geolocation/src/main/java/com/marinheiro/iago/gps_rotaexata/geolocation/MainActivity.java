package com.marinheiro.iago.gps_rotaexata.geolocation;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnIniciarCaptura;
    TextView tvLatitude, tvLongitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tvLatitude = findViewById(R.id.tv_latitude);
        this.tvLongitude = findViewById(R.id.tv_longitude);
        this.btnIniciarCaptura = findViewById(R.id.button_captura);
        this.btnIniciarCaptura.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificarPermissoes();
            }
        });
    }

    private void verificarPermissoes() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else
            configurarLocationManager();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    configurarLocationManager();
                    Toast.makeText(this, "Permissão Concedida!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permissão Recusada!", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public void configurarLocationManager() {
        try {

            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    atualizarInterfaceLatitudeLongitude(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {}

                public void onProviderEnabled(String provider) {}

                public void onProviderDisabled(String provider) {}
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, locationListener);
        } catch (SecurityException ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void atualizarInterfaceLatitudeLongitude(Location location) {
        Double pontoLatitude = location.getLatitude();
        Double pontoLongitude = location.getLongitude();
        tvLatitude.setText(String.valueOf(pontoLatitude));
        tvLongitude.setText(String.valueOf(pontoLongitude));
    }
}

